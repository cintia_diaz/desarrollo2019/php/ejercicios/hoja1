<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         echo __FILE__;
         // ruta donde se encuentra el archivo
         echo "<br>";
         echo __LINE__;
         // numero de lineas que contiene el codigo PHP
         echo "<br>";
         echo PHP_VERSION;
         // version de PHP
         echo "<br>";
         echo PHP_OS;
         // sistema operativo donde trabajamos
         echo "<br>";
         echo __DIR__;
         // directorio donde se encuentra el archivo
         echo "<br>";
        ?>
    </body>
</html>
