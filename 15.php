<?php
    define("ruta", "http://127.0.0.1/cintiaDesarrollo2019/ejercicios/hoja1");
    if(empty($_REQUEST)){
        header("Location: " . ruta . "/15.html");
    }
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
          
         // $_REQUEST es un array asociativo que contiene el contenido de $_GET, $_POST, $_COOKIE
         // extract importa variables a la tabla de símbolos actual desde un array
         extract($_REQUEST);
         echo $nombre;
        ?>
    </body>
</html>
