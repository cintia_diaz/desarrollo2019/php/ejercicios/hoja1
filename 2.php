<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $entero=10;
            $cadena="hola";
            $real=23.6;
            $logico=TRUE;
            
            echo "<pre>";
            var_dump($entero);
            var_dump($cadena);
            var_dump($real);
            var_dump($logico);
            echo "</pre>";
            
            $logico=(int) $logico;
            $entero=(float) $entero;
            settype($logico, "int");
            
            echo "<pre>";
            var_dump($entero);
            var_dump($cadena);
            var_dump($real);
            var_dump($logico);
            echo "</pre>";
        ?>
    </body>
</html>
