<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $datos = array(20, 40, 790, 234, 890);
            $todo = max($datos);
            foreach ($datos as $key => $value){
                $datos[$key] = (int) (100 * $datos[$key] / $todo);
            }
            echo "<pre>";
            var_dump($datos);
            echo "</pre>";
            
            // devuelve el array donde cada item está divido entre el máximo de los items y tomando la parte entera
            
            foreach ($datos as $value){
                for($c=0; $c<$value; $c++){
                    echo "*";
                }
                echo "<br>";
            }

            // devuelve para cada item del array datos, la misma cantidad de * que numeros representa
        ?>
    </body>
</html>
