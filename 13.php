<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            * {
                margin: 0px;
                padding: 0px;
            }
            span {
                width: 10px;
                height: 50px;
                background-color: #001ee7;
                display: inline-block;
            }
            div {
                height: 50px;
                display: inline-block;
                border: 5px solid #CCC;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <?php
           $datos = array(20, 40, 790, 234, 890);
           $todo = max($datos);
           foreach ($datos as $key => $value) {
               $datos[$key] = (int) (100*$datos[$key] / $todo);
           }
           echo "<pre>";
           var_dump($datos);
           echo "</pre>";
           
           /* calcula el maximo de los valores que contiene el array
            * para cada valor del array, multiplica por 100 y divide entre el maximo
            * Almacena la parte entera del cálculo en el array datos
            */
           
           
           foreach ($datos as $value){
               echo "<div>";
               for ($c=0; $c<$value; $c++){
                   echo "<span></span>";
               }
               echo "</div>";
               echo "<br>";
           }
           
           /*
            * Para cada valor del array datos, crea un div con el estilo definido 
            * Dentro del div hay un span cuya anchura la ira dando el valor que representa
            * Se forman 5 divs con anchura cada valor del array y con su estilo definido 
            */
        ?>
    </body>
</html>
