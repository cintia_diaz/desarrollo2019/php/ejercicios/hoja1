<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $alumno1 = "Ramon";
            $alumno2 = "Jose";
            $alumno3 = "Pepe";
            $alumno4 = "Ana";
            
            echo $alumno1;
            echo $alumno2;
            echo "<br>";
            echo $alumno3;
            echo "<br>";
            echo $alumno4;  
            
            // escribe por pantalla 4 variables alumnos con echo
        ?>
                
        <div>
            <?php
                echo "$alumno1<br>$alumno2<br>$alumno3<br>$alumno4";
            ?>
        </div>
        <!-- escribe mediante html 4 alumnos almacenados en variables de php -->
    </body>
</html>
